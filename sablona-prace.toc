\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\contentsline {chapter}{Úvod}{19}{section*.6}% 
\contentsline {chapter}{\numberline {1}Teoretická část studentské práce}{21}{chapter.8}% 
\contentsline {section}{\numberline {1.1}PHpix}{21}{section.9}% 
\contentsline {section}{\numberline {1.2}Šupina snímače}{22}{section.11}% 
\contentsline {section}{\numberline {1.3}FPGA modul}{23}{section.14}% 
\contentsline {section}{\numberline {1.4}Napájení}{24}{section.15}% 
\contentsline {subsection}{\numberline {1.4.1}5V a 1.8V LDO}{25}{subsection.17}% 
\contentsline {subsection}{\numberline {1.4.2}3.3V zdroj pro FPGA}{26}{subsection.25}% 
\contentsline {subsection}{\numberline {1.4.3}66A LT3829}{27}{subsection.34}% 
\contentsline {chapter}{\numberline {2}Výsledky studentské práce}{31}{chapter.38}% 
\contentsline {section}{\numberline {2.1}Fyzická vrstva}{32}{section.40}% 
\contentsline {section}{\numberline {2.2}Propojení fyzické vrstvy a ARM procesoru}{34}{section.44}% 
\contentsline {section}{\numberline {2.3}Software ARM procesoru}{36}{section.47}% 
\contentsline {chapter}{Závěr}{39}{section*.85}% 
\contentsline {chapter}{Literatura}{41}{section*.87}% 
\contentsline {chapter}{Seznam symbol\r {u}, veli\v {c}in a zkratek}{43}{section*.89}% 
\contentsline {chapter}{Seznam p\v {r}\'{\i }loh}{45}{section*.91}% 
\contentsline {chapter}{\numberline {A}Schéma napájení }{48}{chapter.92}% 
\contentsline {chapter}{\numberline {B}FPGA diagram }{55}{chapter.93}% 
